package piglatintranslator_es1;

public class Translator {
	private String phrase;
	public static final String NIL = "nil";
	private int numberOfFirstConsonant;
	private String[] singleWords;
	private String punctuationOccurred;

	public Translator(String inputPhrase) {
		phrase=inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
			
		if(phrase.isEmpty()) {
			return NIL;
		}
				
		if(!startWithVowel()) {
			String tempDxPhrase;
			String newPhrase;
			
			String firstConsonants=separeFirstConsonants();

			tempDxPhrase=phrase.substring(numberOfFirstConsonant);
			newPhrase=tempDxPhrase+firstConsonants+"ay";
			return newPhrase;
		}else { //if starts with vowel
			if (phrase.endsWith("y")) {		
				return phrase+"nay";
			}else if (endWithVowel()) {
				return phrase+"yay";
			}else if(!endWithVowel()){
				return phrase+"ay";
				}
		}
		return phrase;
	
	}
	
	private boolean startWithVowel() {
		return(phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	private boolean endWithVowel() {
		return(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean theLetterIsVowel(int index) {
		return (Character.compare(phrase.charAt(index),'a')==0 || Character.compare(phrase.charAt(index),'e')==0 || Character.compare(phrase.charAt(index),'i')==0 || Character.compare(phrase.charAt(index),'o')==0 || Character.compare(phrase.charAt(index),'u')==0);
	}

	public String separeFirstConsonants() {
		numberOfFirstConsonant=0;
		for(int i=0;i< phrase.length();i++) {
			if(!theLetterIsVowel(i)){
				numberOfFirstConsonant++;
			}else {
				break;
			}
		}
		return phrase.substring(0,numberOfFirstConsonant);
	}

	public String translatePhraseWithMoreWords() {
		String wordJustTranslated;
		String phraseTranslated="";
				
		singleWords = phrase.split(" ");
		if(singleWords.length==1) {
			singleWords = phrase.split("-");
			for (int i=0;i<singleWords.length;i++) {
				phrase=singleWords[i];
				try {
					extractPunctuations();
				} catch (PigLatinException e) {
					phraseTranslated="Not Allowed";
					break;
				}//mi restituisce la punteggiatura trovata e cambia la parola ripulendola dalla punteggiatura 
				wordJustTranslated=this.translate();
				
				if(i!=singleWords.length-1) {
				phraseTranslated=phraseTranslated+wordJustTranslated+"-";
				}else {
					if(punctuationOccurred=="?" || punctuationOccurred=="!") {
						phraseTranslated=phraseTranslated+wordJustTranslated+punctuationOccurred;
					}else {
						phraseTranslated=phraseTranslated+wordJustTranslated;
					}
					
				}
			}
			return phraseTranslated;
		}else {
			for (int i=0;i<singleWords.length;i++) {
				phrase=singleWords[i];
				try {
					extractPunctuations();
				} catch (PigLatinException e) {
					phraseTranslated="Not Allowed";
					break;
				}// cambia la parola ripulendola dalla punteggiatura e valorizza punctuationOccurred cio� la punteggiatura trovata
				wordJustTranslated=this.translate();
				if(i!=singleWords.length-1) {
				phraseTranslated=phraseTranslated+wordJustTranslated+" ";
				}else {					
					if(punctuationOccurred=="!"|| punctuationOccurred=="?" || punctuationOccurred==":"|| punctuationOccurred==";" || punctuationOccurred==","|| punctuationOccurred==")" || punctuationOccurred=="("|| punctuationOccurred=="." || punctuationOccurred=="'" ) {
						phraseTranslated=phraseTranslated+wordJustTranslated+punctuationOccurred;
					}else {
						phraseTranslated=phraseTranslated+wordJustTranslated;
					}				
				}
			}
			return phraseTranslated;
		}
	}
	
	private void extractPunctuations() throws PigLatinException {
					
		if(phrase.indexOf("!")!=-1) {
			removePunctuation("!");
		}else if(phrase.indexOf("?")!=-1) {
			removePunctuation("?");
		}else if(phrase.indexOf(",")!=-1) {
			removePunctuation(",");
		}else if(phrase.indexOf(".")!=-1) {
			removePunctuation(".");
		}else if(phrase.indexOf(";")!=-1) {
			removePunctuation(";");
		}else if(phrase.indexOf(":")!=-1) {
			removePunctuation(":");
		}else if(phrase.indexOf("(")!=-1) {
			removePunctuation("(");
		}else if(phrase.indexOf(")")!=-1) {
			removePunctuation(")");
		}else if(phrase.indexOf("'")!=-1) {
			removePunctuation("'");
		}
		
		if(phrase.contains("]") || phrase.contains("[")){
			throw new PigLatinException();
		}	
		
	}
	
	private void removePunctuation(String punctuation) {
		int indexOfExclamation; 
		
		indexOfExclamation=phrase.indexOf(punctuation);
		phrase.substring(indexOfExclamation);
		phrase=phrase.substring(0,indexOfExclamation);
		punctuationOccurred=punctuation;
	}

}
