package piglatintranslator_es1;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world",translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL,translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithAEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay",translator.translate());
	}
	
	@Test
	public void testTranslationRecognizePhraseMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("kn",translator.separeFirstConsonants());
	}	
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay",translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingMoreWordsSeparatedByDash() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingExclamationMark() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingQuestionMark() {
		String inputPhrase = "hello world?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway?",translator.translatePhraseWithMoreWords());
	}
	
	@Test
	public void testTranslationPhraseContainingPunctuation() {
		String inputPhrase = "hello world;";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway;",translator.translatePhraseWithMoreWords());
	}
	
	@Test 
	public void testTranslationPhraseContainingPunctuationNotAllowed() {
		String inputPhrase = "hello world]";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Not Allowed",translator.translatePhraseWithMoreWords());
	}

}
